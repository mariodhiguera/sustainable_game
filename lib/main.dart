import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sustainable_game/sustainable_game.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Flame.device.fullScreen();
  await Flame.device.setLandscape();

  SustainableGame game = SustainableGame();
  //runApp(GameWidget(game: game));
  runApp(GameWidget(game: kDebugMode ? SustainableGame() : game));
}
