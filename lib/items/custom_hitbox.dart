class CustomHitbox {
  final double offsetx;
  final double offsety;
  final double width;
  final double height;

  CustomHitbox({
    required this.offsetx,
    required this.offsety,
    required this.width,
    required this.height
});
}