import 'dart:async';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:sustainable_game/items/custom_hitbox.dart';
import 'package:sustainable_game/sustainable_game.dart';

class Garbage extends SpriteAnimationComponent
    with HasGameRef<SustainableGame>, CollisionCallbacks {
  final String garbage;
  Garbage({this.garbage = 'Paper', position, size})
      : super(position: position, size: size);

  final double stepTime = 0.05;
  final hitbox = CustomHitbox(offsetx: 10, offsety: 10, width: 12, height: 12);

  @override
  FutureOr<void> onLoad() {
    priority = 0;
    //debugMode = true;

    add(RectangleHitbox(
        position: Vector2(hitbox.offsetx, hitbox.offsety),
        size: Vector2(hitbox.width, hitbox.height),
        collisionType: CollisionType.passive));
    animation = SpriteAnimation.fromFrameData(
        game.images.fromCache('Items/Garbages/$garbage.png'),
        SpriteAnimationData.sequenced(
            amount: 17, stepTime: stepTime, textureSize: Vector2.all(32)));
    return super.onLoad();
  }

  void collidedWithPlayer() {
    print('collding with player');
    removeFromParent();
  }
}
