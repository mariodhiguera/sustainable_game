import 'dart:async';

import 'package:flame/components.dart';
import 'package:flame_tiled/flame_tiled.dart';
import 'package:flame_tiled/flame_tiled.dart';
import 'package:sustainable_game/actors/player.dart';

import '../items/garbage.dart';

class Level extends World{

  final String levelName;
  final Player player;

  Level({required this.levelName, required this.player});

  late TiledComponent level;

  @override
  FutureOr<void> onLoad() async{

    level = await TiledComponent.load('$levelName.tmx', Vector2.all(16));
    add(level);

    _spawningObjects();

    return super.onLoad();
  }

  void _spawningObjects() {

    final spawnPointsLayer = level.tileMap.getLayer<ObjectGroup>('Spawnpoints');

    if(spawnPointsLayer != null) {
      for (final spawnPoints in spawnPointsLayer.objects) {
        switch (spawnPoints.class_) {
          case 'Player' :
            player.position = Vector2(spawnPoints.x, spawnPoints.y);
            add(player);
            break;
          case 'Garbage' :
            final garbage = Garbage(garbage: spawnPoints.name, position: Vector2(spawnPoints.x, spawnPoints.y), size: Vector2(spawnPoints.width, spawnPoints.height) );
            add(garbage);
            break;
          default:
        }
      }
    }
  }

}