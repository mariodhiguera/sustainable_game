import 'dart:async';
import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flame/game.dart';
import 'package:sustainable_game/actors/player.dart';
import 'package:sustainable_game/levels/level.dart';

class SustainableGame extends FlameGame with HasKeyboardHandlerComponents, DragCallbacks, HasCollisionDetection {

  @override
  Color backgroundColor() => const Color(0xFF211F30);


  late final CameraComponent cam;
  Player player = Player(character: 'Ninja Frog');

  @override
  FutureOr<void> onLoad() async {
    
    await images.loadAllImages();

    final world = Level(player: player, levelName: 'Level-03');

    cam = CameraComponent.withFixedResolution(world: world, width: 640, height: 360);
    cam.viewfinder.anchor = Anchor.topLeft;

    addAll([cam, world]);

    return super.onLoad();
  }

  bool tappedComponent = false;
/*
  @override
  void onTapDown(int pointerId, TapDownInfo info) {
    super.onTapDown(pointerId, info);
    if(!tappedComponent) {
      // Will add a component at the position that was tapped
      add(MyComponent(info.eventPosition.game));
    }
  }

  @override
  void onTapUp(int pointerId, TapUpInfo info) {
    super.onTapDown(pointerId, info);
    tappedComponent = false;
  }*/
}