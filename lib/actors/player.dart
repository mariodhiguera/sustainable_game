import 'dart:async';

import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flutter/services.dart';
import 'package:sustainable_game/items/custom_hitbox.dart';
import 'package:sustainable_game/items/garbage.dart';
import 'package:sustainable_game/sustainable_game.dart';

enum PlayerState {idle, running}
enum PlayerDirection{right, left, none}

class Player extends SpriteAnimationGroupComponent
    with HasGameRef<SustainableGame>, KeyboardHandler, CollisionCallbacks {

  String character;
  Player({position, this.character = 'Ninja Frog'}) : super(position: position);

  late final SpriteAnimation idleAnimation;
  late final SpriteAnimation runningAnimation;
  final double stepTime = 0.05;

  PlayerDirection playerDirection = PlayerDirection.none;
  double moveSpeed = 100;
  Vector2 velocity = Vector2.zero();
  bool isFacingRight = true;
  CustomHitbox hitbox = CustomHitbox(
    offsetx: 10,
    offsety: 4,
    width: 14,
    height: 28
  );

  @override
  FutureOr<void> onLoad() {
    // TODO: implement onLoad
    priority = 1;
    _loadAllAnimations();
    return super.onLoad();
  }

  @override
  void update(double dt) {
    _updatePlayerMovement(dt);
    super.update(dt);
  }

  @override
  bool onKeyEvent(RawKeyEvent event, Set<LogicalKeyboardKey> keysPressed) {
    //print('Key'+event.toString());

    final isRightKeyPressed = keysPressed.contains(LogicalKeyboardKey.keyD) || keysPressed.contains(LogicalKeyboardKey.arrowRight);
    final isLeftKeyPressed = keysPressed.contains(LogicalKeyboardKey.keyA) || keysPressed.contains(LogicalKeyboardKey.arrowLeft);

    if(isLeftKeyPressed && isRightKeyPressed){
      //print('none');
      playerDirection = PlayerDirection.none;
    }else if(isRightKeyPressed){
      //print('right');
      playerDirection = PlayerDirection.right;
    }else if(isLeftKeyPressed){
      //print('left');
      playerDirection = PlayerDirection.left;
    }else{
      //print('Others');
      playerDirection = PlayerDirection.none;
    }

    return super.onKeyEvent(event, keysPressed);
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    if(other is Garbage){
      other.collidedWithPlayer();
      //print('colliding');
    }
    super.onCollision(intersectionPoints, other);
  }

  void _loadAllAnimations() {

    idleAnimation = _spriteAnimation('Idle', 11);
    runningAnimation = _spriteAnimation('Run', 12);

    //List of all animations
    animations = {
      PlayerState.idle : idleAnimation,
      PlayerState.running : runningAnimation,
    };

    //Set current animations
    current = PlayerState.idle;
  }

  SpriteAnimation _spriteAnimation(String state, int amount){
    return SpriteAnimation.fromFrameData(
        game.images.fromCache('Main Characters/$character/$state (32x32).png'),
        SpriteAnimationData.sequenced(
            amount: amount, stepTime: stepTime, textureSize: Vector2.all(32)));
  }

  void _updatePlayerMovement(double dt) {
    double dirX = 0.0;

    switch (playerDirection){
      case PlayerDirection.right:
        if(!isFacingRight){
          flipHorizontallyAroundCenter();
          isFacingRight = true;
        }
        current = PlayerState.running;
        dirX += moveSpeed;
        break;
      case PlayerDirection.left:
        if(isFacingRight){
          flipHorizontallyAroundCenter();
          isFacingRight = false;
        }
        current = PlayerState.running;
        dirX -= moveSpeed;
        break;
      case PlayerDirection.none:
        current = PlayerState.idle;
        break;
      default:
    }

    velocity = Vector2(dirX, 0.0);
    position += velocity * dt;
  }


}
